packageManager=pacman
packageManagerConfirm=--noconfirm
packageManagerInstall=$packageManager\ -Sy
user=amnesia
homePath=/home/$user

git clone https://github.com/flutter/flutter.git -b stable $homePath/dev/src/flutter
echo "# personal">>$homePath/.bashrc
echo 'export PATH="$PATH:~/dev/src/flutter/bin"'>>$homePath/.bashrc
$packageManagerInstall clang $packageManagerConfirm
$packageManagerInstall gtk3 $packageManagerConfirm
$packageManagerInstall cmake $packageManagerConfirm
$packageManagerInstall ninja $packageManagerConfirm
$packageManagerInstall pkg-config $packageManagerConfirm
$packageManagerInstall unzip $packageManagerConfirm
$packageManagerInstall curl $packageManagerConfirm

chown -R $user $homePath
