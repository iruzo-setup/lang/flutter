flutter config --no-analytics
flutter config --no-enable-windows-desktop
flutter config --no-enable-web
flutter config --no-enable-android
flutter config --no-enable-ios
flutter config --no-enable-fuchsia
flutter config --no-enable-macos-desktop
flutter config --no-enable-linux-desktop
flutter doctor
